package no.martinsolaas.apiclient.kassalapp.products;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jakarta.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "brand",
        "vendor",
        "ean",
        "url",
        "image",
        "category",
        "description",
        "ingredients",
        "current_price",
        "current_unit_price",
        "weight",
        "weight_unit",
        "store",
        "price_history",
        "allergens",
        "nutrition",
        "labels",
        "created_at",
        "updated_at"
})
@Generated("jsonschema2pojo")
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("brand")
    private Object brand;
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("ean")
    private String ean;
    @JsonProperty("url")
    private String url;
    @JsonProperty("image")
    private String image;
    @JsonProperty("category")
    @Valid
    private List<Category> category = new ArrayList<Category>();
    @JsonProperty("description")
    private Object description;
    @JsonProperty("ingredients")
    private Object ingredients;
    @JsonProperty("current_price")
    private Integer currentPrice;
    @JsonProperty("current_unit_price")
    private Object currentUnitPrice;
    @JsonProperty("weight")
    private Object weight;
    @JsonProperty("weight_unit")
    private Object weightUnit;
    @JsonProperty("store")
    @Valid
    private Store store;
    @JsonProperty("price_history")
    @Valid
    private List<PriceHistory> priceHistory = new ArrayList<PriceHistory>();
    @JsonProperty("allergens")
    @Valid
    private List<Allergen> allergens = new ArrayList<Allergen>();
    @JsonProperty("nutrition")
    @Valid
    private List<Nutrition> nutrition = new ArrayList<Nutrition>();
    @JsonProperty("labels")
    @Valid
    private List<Label> labels = new ArrayList<Label>();
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Datum withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Datum withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("brand")
    public Object getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public Datum withBrand(Object brand) {
        this.brand = brand;
        return this;
    }

    @JsonProperty("vendor")
    public String getVendor() {
        return vendor;
    }

    @JsonProperty("vendor")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Datum withVendor(String vendor) {
        this.vendor = vendor;
        return this;
    }

    @JsonProperty("ean")
    public String getEan() {
        return ean;
    }

    @JsonProperty("ean")
    public void setEan(String ean) {
        this.ean = ean;
    }

    public Datum withEan(String ean) {
        this.ean = ean;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public Datum withUrl(String url) {
        this.url = url;
        return this;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public Datum withImage(String image) {
        this.image = image;
        return this;
    }

    @JsonProperty("category")
    public List<Category> getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public Datum withCategory(List<Category> category) {
        this.category = category;
        return this;
    }

    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Object description) {
        this.description = description;
    }

    public Datum withDescription(Object description) {
        this.description = description;
        return this;
    }

    @JsonProperty("ingredients")
    public Object getIngredients() {
        return ingredients;
    }

    @JsonProperty("ingredients")
    public void setIngredients(Object ingredients) {
        this.ingredients = ingredients;
    }

    public Datum withIngredients(Object ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    @JsonProperty("current_price")
    public Integer getCurrentPrice() {
        return currentPrice;
    }

    @JsonProperty("current_price")
    public void setCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Datum withCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
        return this;
    }

    @JsonProperty("current_unit_price")
    public Object getCurrentUnitPrice() {
        return currentUnitPrice;
    }

    @JsonProperty("current_unit_price")
    public void setCurrentUnitPrice(Object currentUnitPrice) {
        this.currentUnitPrice = currentUnitPrice;
    }

    public Datum withCurrentUnitPrice(Object currentUnitPrice) {
        this.currentUnitPrice = currentUnitPrice;
        return this;
    }

    @JsonProperty("weight")
    public Object getWeight() {
        return weight;
    }

    @JsonProperty("weight")
    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public Datum withWeight(Object weight) {
        this.weight = weight;
        return this;
    }

    @JsonProperty("weight_unit")
    public Object getWeightUnit() {
        return weightUnit;
    }

    @JsonProperty("weight_unit")
    public void setWeightUnit(Object weightUnit) {
        this.weightUnit = weightUnit;
    }

    public Datum withWeightUnit(Object weightUnit) {
        this.weightUnit = weightUnit;
        return this;
    }

    @JsonProperty("store")
    public Store getStore() {
        return store;
    }

    @JsonProperty("store")
    public void setStore(Store store) {
        this.store = store;
    }

    public Datum withStore(Store store) {
        this.store = store;
        return this;
    }

    @JsonProperty("price_history")
    public List<PriceHistory> getPriceHistory() {
        return priceHistory;
    }

    @JsonProperty("price_history")
    public void setPriceHistory(List<PriceHistory> priceHistory) {
        this.priceHistory = priceHistory;
    }

    public Datum withPriceHistory(List<PriceHistory> priceHistory) {
        this.priceHistory = priceHistory;
        return this;
    }

    @JsonProperty("allergens")
    public List<Allergen> getAllergens() {
        return allergens;
    }

    @JsonProperty("allergens")
    public void setAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    public Datum withAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
        return this;
    }

    @JsonProperty("nutrition")
    public List<Nutrition> getNutrition() {
        return nutrition;
    }

    @JsonProperty("nutrition")
    public void setNutrition(List<Nutrition> nutrition) {
        this.nutrition = nutrition;
    }

    public Datum withNutrition(List<Nutrition> nutrition) {
        this.nutrition = nutrition;
        return this;
    }

    @JsonProperty("labels")
    public List<Label> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public Datum withLabels(List<Label> labels) {
        this.labels = labels;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Datum withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Datum withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Datum withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}