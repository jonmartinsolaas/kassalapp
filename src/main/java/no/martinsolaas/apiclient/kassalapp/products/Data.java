package no.martinsolaas.apiclient.kassalapp.products;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jakarta.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ean",
        "products",
        "allergens",
        "nutrition",
        "labels"
})
@Generated("jsonschema2pojo")
public class Data {

    @JsonProperty("ean")
    private String ean;
    @JsonProperty("products")
    @Valid
    private List<Products> products = new ArrayList<Products>();
    @JsonProperty("allergens")
    @Valid
    private List<Allergen> allergens = new ArrayList<Allergen>();
    @JsonProperty("nutrition")
    @Valid
    private List<Nutrition> nutrition = new ArrayList<Nutrition>();
    @JsonProperty("labels")
    @Valid
    private List<Object> labels = new ArrayList<Object>();
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("ean")
    public String getEan() {
        return ean;
    }

    @JsonProperty("ean")
    public void setEan(String ean) {
        this.ean = ean;
    }

    @JsonProperty("products")
    public List<Products> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Products> products) {
        this.products = products;
    }

    @JsonProperty("allergens")
    public List<Allergen> getAllergens() {
        return allergens;
    }

    @JsonProperty("allergens")
    public void setAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    @JsonProperty("nutrition")
    public List<Nutrition> getNutrition() {
        return nutrition;
    }

    @JsonProperty("nutrition")
    public void setNutrition(List<Nutrition> nutrition) {
        this.nutrition = nutrition;
    }

    @JsonProperty("labels")
    public List<Object> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<Object> labels) {
        this.labels = labels;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}