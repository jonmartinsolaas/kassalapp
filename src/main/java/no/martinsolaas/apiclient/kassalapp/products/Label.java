package no.martinsolaas.apiclient.kassalapp.products;

import java.util.LinkedHashMap;
import java.util.Map;
import jakarta.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "display_name",
        "description",
        "organization",
        "alternative_names",
        "type",
        "year_established",
        "about",
        "note",
        "icon"
})
@Generated("jsonschema2pojo")
public class Label {

    @JsonProperty("name")
    private String name;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("organization")
    private String organization;
    @JsonProperty("alternative_names")
    private Object alternativeNames;
    @JsonProperty("type")
    private String type;
    @JsonProperty("year_established")
    private Integer yearEstablished;
    @JsonProperty("about")
    private String about;
    @JsonProperty("note")
    private String note;
    @JsonProperty("icon")
    @Valid
    private Icon icon;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Label withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("display_name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Label withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Label withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("organization")
    public String getOrganization() {
        return organization;
    }

    @JsonProperty("organization")
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Label withOrganization(String organization) {
        this.organization = organization;
        return this;
    }

    @JsonProperty("alternative_names")
    public Object getAlternativeNames() {
        return alternativeNames;
    }

    @JsonProperty("alternative_names")
    public void setAlternativeNames(Object alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public Label withAlternativeNames(Object alternativeNames) {
        this.alternativeNames = alternativeNames;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Label withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("year_established")
    public Integer getYearEstablished() {
        return yearEstablished;
    }

    @JsonProperty("year_established")
    public void setYearEstablished(Integer yearEstablished) {
        this.yearEstablished = yearEstablished;
    }

    public Label withYearEstablished(Integer yearEstablished) {
        this.yearEstablished = yearEstablished;
        return this;
    }

    @JsonProperty("about")
    public String getAbout() {
        return about;
    }

    @JsonProperty("about")
    public void setAbout(String about) {
        this.about = about;
    }

    public Label withAbout(String about) {
        this.about = about;
        return this;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("note")
    public void setNote(String note) {
        this.note = note;
    }

    public Label withNote(String note) {
        this.note = note;
        return this;
    }

    @JsonProperty("icon")
    public Icon getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Label withIcon(Icon icon) {
        this.icon = icon;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Label withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}