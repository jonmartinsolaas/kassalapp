package no.martinsolaas.apiclient.kassalapp.physicalstores;

import java.util.LinkedHashMap;
import java.util.Map;
import jakarta.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "sunday"
})
@Generated("jsonschema2pojo")
public class OpeningHours {

    @JsonProperty("monday")
    private String monday;
    @JsonProperty("tuesday")
    private String tuesday;
    @JsonProperty("wednesday")
    private String wednesday;
    @JsonProperty("thursday")
    private String thursday;
    @JsonProperty("friday")
    private String friday;
    @JsonProperty("saturday")
    private String saturday;
    @JsonProperty("sunday")
    private Object sunday;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    @JsonProperty("monday")
    public String getMonday() {
        return monday;
    }

    @JsonProperty("monday")
    public void setMonday(String monday) {
        this.monday = monday;
    }

    @JsonProperty("tuesday")
    public String getTuesday() {
        return tuesday;
    }

    @JsonProperty("tuesday")
    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    @JsonProperty("wednesday")
    public String getWednesday() {
        return wednesday;
    }

    @JsonProperty("wednesday")
    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    @JsonProperty("thursday")
    public String getThursday() {
        return thursday;
    }

    @JsonProperty("thursday")
    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    @JsonProperty("friday")
    public String getFriday() {
        return friday;
    }

    @JsonProperty("friday")
    public void setFriday(String friday) {
        this.friday = friday;
    }

    @JsonProperty("saturday")
    public String getSaturday() {
        return saturday;
    }

    @JsonProperty("saturday")
    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    @JsonProperty("sunday")
    public Object getSunday() {
        return sunday;
    }

    @JsonProperty("sunday")
    public void setSunday(Object sunday) {
        this.sunday = sunday;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}